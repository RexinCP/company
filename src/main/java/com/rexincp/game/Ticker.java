package com.rexincp.game;

/**
 * Tickable
 * An interface which ticks at a constant rate which entities and tasks can complete concurrently
 * @author Ryan Wilson (@RexinCP)
 */
public interface Ticker {

    /**
     * On tick method is called every x time
     * @param tick Current game tick.
     */
    void onTick(long tick);
}
