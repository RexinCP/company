package com.rexincp.game.active;

import com.rexincp.game.Ticker;

/**
 * An abstract class which can be used by classes which require a ticker
 * so that managers can assign jobs to entities to complete
 * @author Ryan Wilson (@RexinCP)
 */
public abstract class Job implements Ticker {
    protected boolean started = false; // if job has started
    protected boolean complete = false; // If job is complete
    protected boolean alive = false; // If job is alive
    protected boolean paused = false; // If job is paused (by higher priority)
    protected int priority; // Can be overidden by higher priority.

    // TODO Implement entity Job is applied to

    public Job(int priority) {
        this.priority = priority;
    };

    /**
     * Check if Job is complete
     * @return If job is complete
     */
    public boolean isComplete() {return complete;}

    /**
     * Check if job is still alive
     * @return Job is alive
     */
    public boolean isAlive() {return alive;}

    /**
     * Check if job is paused
     * @return
     */
    public boolean isPaused() {return paused;}

    /**
     * Start the Job
     */
    public void start() {
        started = true;
        paused = false;
        alive = true;
    }

    /**
     * Pause the Job
     */
    public void pause() {
        paused = true;
    }

    /**
     * Complete the job and kill it
     */
    public void finish() {
        complete = true;
        kill();
    }

    /**
     * Kill the Job
     */
    public void kill() {alive = false;}
}
