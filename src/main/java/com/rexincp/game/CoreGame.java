package com.rexincp.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.rexincp.game.screens.GameScreen;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * CoreGame class which controls the changing between game screens.
 */
public class CoreGame extends Game {

    private static final Logger LOGGER = LoggerFactory.getLogger(CoreGame.class);
    private SpriteBatch batch;
    private Screen screen;

    @Override
    public void create() {
        // initialise screen
        screen = new GameScreen();
        super.setScreen(screen);

        // Initialise batch
        batch = new SpriteBatch();
        LOGGER.info("Running Game Screen...");
    }

    @Override
    public void dispose() {
        LOGGER.info("Exiting application.");
        screen.dispose();
        batch.dispose();
    }
}
