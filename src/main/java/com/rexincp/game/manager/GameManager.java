package com.rexincp.game.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.rexincp.game.Ticker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Class which deals with all the main Game's information and tasks.
 * This will hold global variables and settings which everything in
 * the game will require.
 * throughout the game screens and the CoreGame class.
 *
 * @author Ryan Wilson (@RexinCP)
 */
public class GameManager implements Manager {

    // Variables
    private static final Logger LOGGER = LoggerFactory.getLogger(GameManager.class);
    private static GameManager instance;
    private int generatedChunks = 0;
    private static long calls = 0; // Number of times instance has been called.
    private long gameTime = 0; // Game time
    private List<Manager> managers = new ArrayList<>();
    private Screen currentScreen;
    private OrthographicCamera camera;

    /**
     * Constructor (NULL)
     */
    public GameManager() {}

    /**
     * Get the instance of GameManager
     * @return instance
     */
    public static GameManager get() {
        calls ++;
        if (instance == null) {
            instance = new GameManager();
            LOGGER.info("Initialising GameManager (First call).");
        }
        return instance;
    }

    /**
     * For debugging purposes:
     * @return number of times the instance has been called
     */
    public long getCalls() {
        return this.calls;
    }

    /**
     * Initialises the game information
     * @param cam Camera
     */
    public void initGame(OrthographicCamera cam) {
        LOGGER.info("Initialising game.");

        camera = cam;
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    /**
     * Get's a manager attached to GameManager
     * @param manager Class of manager
     * @return Manager (null if not initialised)
     */
    public <T> T getManager(Class<T> manager) {
        for (Manager m : managers) {
            if (m.getClass().equals(manager)) {
                return (T) m;
            }
        }
        return null;
    }

    /**
     * Adds a manager to the GameManager instance
     * @param manager
     * @return
     */
    public boolean addManager(Manager manager) {
        for (Manager m : managers) {
            if (m.getClass().equals(manager.getClass())) {
                return false;
            }
        }
        managers.add(manager);
        LOGGER.info("Added manager successfully! (" + manager.getClass().toString() + ")");
        return true;
    }

    public void onTick() {
        gameTime ++;
        for (Manager m : managers) {
            if (m instanceof Ticker)
                ((Ticker) m).onTick(gameTime);
        }
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public void setScreen(Screen screen) {
        currentScreen = screen;
    }

    public void alterGeneratedChunks(int i) {
        generatedChunks += i;
    }

    public int getGeneratedChunks() {
        return generatedChunks;
    }
}
