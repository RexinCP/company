package com.rexincp.game.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.rexincp.game.objects.TileType;
import com.rexincp.game.util.Direction;
import com.rexincp.game.world.Biome;
import com.rexincp.game.world.Chunk;
import com.rexincp.game.world.ChunkInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

/**
 * A class to take care of generating, changing, and retrieving data from the
 * world and to decide when to use certain tasks. Needs to keep count of how
 * many chunks have been generated, all the world data, how many chunks are
 * to be loaded, etc.
 *
 * @author Ryan Wilson (@RexinCP)
 */
public class WorldManager implements Manager {

    private final Logger LOGGER = LoggerFactory.getLogger(WorldManager.class);
    List<Chunk> world = new LinkedList<Chunk>();
    private Chunk originalChunk = null;
    private int generatedChunks = 0;
    private int loadedChunks = 0;
    private int leftBiomeCount = 0;
    private int rightBiomeCount = 0;
    private int worldWidth = 0;
    private int originalChunkPosition = 0;

    /**
     * World render method
     * @param camera camera to render to
     * @param batch Batch to draw
     */
    public void render(OrthographicCamera camera, SpriteBatch batch) {
        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        batch.draw(TileType.SKY_GRADIENT.getTexture(), 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        for (int row = 0; row < ChunkInfo.getHEIGHT(); row++) {
            for (int chunk = 0; chunk < world.size(); chunk++) {
                for (int col = 0; col < ChunkInfo.getWIDTH(); col++) {
                    TileType type = this.getTileTypeByCoordinate(1, chunk, col, row);
                    if (type != null && type.getId() != 0)
                        batch.draw(type.getTexture(), (col * TileType.TILE_SIZE + (chunk - originalChunkPosition) *
                                ChunkInfo.getPixelWidth()), row * TileType.TILE_SIZE);
                }
            }
        }

        batch.end();
    }

    /**
     * Get the world coordinate (x and y)
     * @param screenX x position on un-projected screen
     * @param screenY y position on un-projected screen
     * @return Vector2 containing world x and y position
     */
    public Vector2 getWorldCoordinate(float screenX, float screenY) {
        int col = (int) Math.floor(screenX / TileType.TILE_SIZE);
        int row = (int) Math.floor(screenY / TileType.TILE_SIZE);
        return new Vector2(col, row);
    }


    /**
     * Get the world coordinate from the 0th chunk (x, y, chunk)
     * This appropriately gets the correct chunk number to reference the
     * world data
     * @param vector The vector containing the x and y position in world.
     * @return World Position
     */
    public Vector3 getExactWorldDataCoordinate(Vector2 vector) {
        int chunk = (int) Math.floor(vector.x / ChunkInfo.getWIDTH() + originalChunkPosition);
        int new_col = (int) vector.x - (ChunkInfo.getWIDTH() * chunk);
        return new Vector3(new_col, vector.y, chunk);
    }

    /**
     * Get the world coordinate (x, y, chunk)
     * @param screenX screen position x
     * @param screenY screen position y
     * @return World Position
     */
    public Vector3 getWorldDataCoordinate(float screenX, float screenY) {
        int chunk = (int) Math.floor(screenX / ChunkInfo.getPixelWidth());
        int col = (int) Math.floor(screenX / TileType.TILE_SIZE) - (ChunkInfo.getWIDTH() * chunk);
        int row = (int) Math.floor(screenY / TileType.TILE_SIZE);
        return new Vector3(col, row, chunk);
    }

    /**
     * Get the world coordinate (x, y, chunk)
     * @param vector Vector containing col and row
     * @return World Position
     */
    public Vector3 getWorldDataCoordinate(Vector2 vector) {
        int chunk = (int) Math.floor(vector.x / ChunkInfo.getWIDTH());
        int new_col = (int) vector.x - (ChunkInfo.getWIDTH() * chunk);
        return new Vector3(new_col, vector.y, chunk);
    }


    /**
     * Get the block at a given location where the mouse is
     * @param layer Layer on screen
     * @param screenX screen position x
     * @param screenY screen position y
     * @return Tile at the location
     */
    public TileType getTileTypeByLocation(int layer, float screenX, float screenY) {
        Vector3 vec = getWorldDataCoordinate(screenX, screenY);
        vec.z += originalChunkPosition;

        return getTileTypeByCoordinate(layer, (int) vec.z, (int) vec.x, (int) vec.y);
    }

    /**
     * Get the block at the given position
     * @param layer Layer
     * @param chunk Chunk
     * @param col Col
     * @param row Row
     * @return Tile at the location
     */
    public TileType getTileTypeByCoordinate(int layer, int chunk, int col, int row) {
        if (col < 0 || col >= worldWidth || row < 0 || row >= 100)
            return TileType.BLANK;
        return TileType.getTileTypeById(world.get(chunk).getData()[layer][row][col]);
    }

    /**
     * Generate a new chunk in a specific direction from the last chunk
     * @param direction LEFT, RIGHT, or INVALID (If the first chunk)
     */
    public void generateNewChunk(Direction direction) {

        Chunk newChunk = new Chunk(Biome.PLAIN, null, null);

        switch (direction) {
            case LEFT:
                originalChunkPosition++;
                addToStart(newChunk);
                break;
            case RIGHT:
                world.add(newChunk);
                break;
            case INVALID:
                world.add(newChunk);
                break;
            default:
                LOGGER.error("Need to give the direction as LEFT, RIGHT, or INVALID");
        }

        generatedChunks++;
        worldWidth += ChunkInfo.getWIDTH();

        if (originalChunk == null) {
            originalChunk = newChunk;
            originalChunkPosition = 0;
        }
    }

    /**
     * Get the width of the world (in blocks)
     * @return World width
     */
    public int getWorldWidth() {
        return worldWidth;
    }

    /**
     * Get the size of the world in pixels
     * @return World Width (Pixels)
     */
    public int getWorldWidthPixels() {
        return getWorldWidth() * TileType.TILE_SIZE;
    }

    /**
     * Get the position of the original chunk in the list of chunks
     * @return Original chunk position.
     */
    public int getOriginalChunkPosition() {
        return originalChunkPosition;
    }

    /**
     * Get the total amount of generated chunks aat the present time
     * @return Amount of generated chunks
     */
    public int getGeneratedChunks() {
        return generatedChunks;
    }

    /**
     * Add a newchunk to the start of the list of chunks,
     * then move each one ahead by one
     * @param new_chunk New chunk to add
     */
    private void addToStart(Chunk new_chunk) {
        Chunk prev = new_chunk;
        world.add(prev);
        for (int iterator = 0; iterator < world.size(); iterator++) {
            prev = world.set(iterator, prev);
        }
    }
}
