package com.rexincp.game.manager;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.World;
import com.rexincp.game.graphics.Performance;
import com.rexincp.game.util.Direction;
import com.rexincp.game.world.ChunkInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for handling user input
 * E.g. Button presses, mouse clicks, etc.
 *
 * @author Ryan Wilson (@RexinCP)
 */
public class InputManager implements Manager, InputProcessor {

    private final Logger LOGGER = LoggerFactory.getLogger(InputManager.class);
    private OrthographicCamera cam;
    private boolean typing = false;
    private Object Performance;

    public InputManager(OrthographicCamera cam) {
        this.cam = cam;
    }

    @Override
    public boolean keyDown(int keycode) {
        // check if user is typing in chat
        if (!typing) {
            switch (keycode) {
                case Input.Keys.EQUALS:
                    cam.zoom = 20;
                    break;
                case Input.Keys.NUM_1:
                    cam.position.set(0, 0, 0);
                    break;
                case Input.Keys.F3:
                    Performance p = GameManager.get().getManager(Performance.class);

                    if (p.getDisplay()) {
                        p.setDisplay(false);
                    } else {
                        p.setDisplay(true);
                    }
                    break;
                default:
                    LOGGER.info("Key doesn't have a role.");
            }
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        // TODO this is only for debugging purposes
        cam.translate(-(Gdx.input.getDeltaX() * cam.zoom), Gdx.input.getDeltaY() * cam.zoom);
        checkCamera();
        cam.update();
        return true;
    }

    private Vector3 lastpos = null;
    private int lastposBlock = 0;

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        Vector3 mousePosition = getMousePositionBlock(screenX, screenY);
        Performance p = GameManager.get().getManager(Performance.class);
        WorldManager wm = GameManager.get().getManager(WorldManager.class);
        Vector2 wc = wm.getWorldCoordinate(mousePosition.x, mousePosition.y);
        Vector3 wdc = wm.getWorldDataCoordinate(wc);

        p.setMouseScreenPosition(mousePosition.x, mousePosition.y);
        p.setWorldCoordinates(wc);
        p.setMouseWorldPosition((int) wdc.x, (int) wdc.y, (int) wdc.z);
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        if (amount == 1) {
            cam.zoom += 0.1f;
            if (cam.zoom > 2) {
                cam.zoom = 2;
            }
        } else {

            if (cam.zoom < 1) {
                cam.zoom = 1;
            }
            cam.zoom -= 0.1f;
        }
        checkCamera();
        return false;
    }

    /**
     * Function to check if the camera has hit the edge of the world.
     * If it has, then make sure the camera cannot move past the edge of the world.
     * TODO Instead of setting cam position, render another world chunk on x-axis.
     */
    public void checkCamera() {
        // Variables for checking camera bounds
        float camx = Gdx.graphics.getWidth()/2 * cam.zoom;
        float camy = Gdx.graphics.getHeight()/2 * cam.zoom;
        WorldManager world = GameManager.get().getManager(WorldManager.class);

        // Check left-side of world
        if (cam.position.x - camx < -world.getOriginalChunkPosition() * ChunkInfo.getPixelWidth()) {
            world.generateNewChunk(Direction.LEFT);
        }
        // Check right-side of world
        if (cam.position.x + camx > ((world.getGeneratedChunks() - world.getOriginalChunkPosition()) *
                ChunkInfo.getPixelWidth())) {
            world.generateNewChunk(Direction.RIGHT);
        }
        // Check top-side of world
        if (cam.position.y + camy > ChunkInfo.getPixelHeight())
            cam.position.y = ChunkInfo.getPixelHeight() - camy;

        // Check bottom-side of world
        if (cam.position.y - camy < 0)
            cam.position.y = camy;

        cam.update();

    }

    /**
     * Get's the mouse position via world coordinates.
     * @param screenX x Screen position of mouse
     * @param screenY y Screen position of mouse
     * @return Vector3 of world coordinate
     */
    public Vector3 getMousePositionBlock(int screenX, int screenY) {
        Vector3 mouseWorld = new Vector3(screenX, screenY, 0);
        return cam.unproject(mouseWorld);
    }
}
