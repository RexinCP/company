package com.rexincp.game.entities;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.rexincp.game.Ticker;

public class Mob extends Entity implements Ticker {


    Mob(float x, float y, EntityType type, boolean grounded) {
        super(x, y, type, grounded);
    }

    @Override
    public void onTick(long tick) {

    }

    @Override
    public void render(SpriteBatch batch) {

    }
}
