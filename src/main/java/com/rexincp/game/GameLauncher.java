package com.rexincp.game;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GameLauncher {

    private static final Logger LOGGER = LoggerFactory.getLogger(GameLauncher.class);

    public static void main (String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Company: A new adventure";
        config.width = 1920;
        config.height = 1080;
        config.foregroundFPS = 60;
        config.backgroundFPS = 10;
        new LwjglApplication(new CoreGame(), config);
        LOGGER.info("Launching application...");
    }

}