package com.rexincp.game.graphics;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.rexincp.game.manager.GameManager;
import com.rexincp.game.manager.Manager;
import com.rexincp.game.manager.WorldManager;

/**
 * A class for displaying game information on screen
 * This class is useful to see data and determine
 * what / if anything is going wrong.
 *
 * @author Ryan Wilson (@RexinCP)
 */
public class Performance implements Manager {

    private int chunk = 0, col = 0, row = 0;
    private int worldCol = 0, worldRow = 0;
    private float screenX = 0, screenY = 0;
    private FrameRate fr;
    private int nl;

    private BitmapFont font;
    private SpriteBatch batch;
    private boolean display; // Whether to display info

    /**
     * Constructor
     */
    public Performance() {
        this.display = false;
        this.font = new BitmapFont();
        this.batch = new SpriteBatch();
        fr = new FrameRate();
    }

    /**
     * Set the Mouse Position in the world
     * @param col Column of the mouse
     * @param row Row of the mouse
     * @param chunk Chunk the mouse is in
     */
    public void setMouseWorldPosition(int col, int row, int chunk) {
        this.chunk = chunk;
        this.col = col;
        this.row = row;
    }

    /**
     * Set the world coordinates (x, y)
     * @hint Use WorldManager's getWorldCoordinate
     * @param vector Vector holding the coordinates
     */
    public void setWorldCoordinates(Vector2 vector) {
        this.worldCol = (int) vector.x;
        this.worldRow = (int) vector.y;
    }

    /**
     * Set the Mouse screen position
     * @param screenX Screen position (Unprojected)
     * @param screenY Screen Position (Unprojected)
     */
    public void setMouseScreenPosition(float screenX, float screenY) {
        this.screenX = screenX;
        this.screenY = screenY;
    }

    /**
     * Display Performance information when 'F3' is pressed
     * @param display True or False
     */
    public void setDisplay(boolean display) {
        this.display = display;
    }

    /**
     * Get Display status
     * @return Display
     */
    public boolean getDisplay() {
        return this.display;
    }

    /**
     * Render method - Write information to screen.
     */
    public void render() {
        if (display) {

            fr.update();
            batch.begin();
            nl = Gdx.graphics.getHeight() - 3;
            int op = GameManager.get().getManager(WorldManager.class).getOriginalChunkPosition();

            font.draw(batch, fr.getFrameRate() + " fps", 3, nl);
            font.draw(batch, "World Mouse Position: Chunk " + chunk + ", Col " +
                    col + ", Row " + row, 3, nl -= 20);
            font.draw(batch, "World Mouse Coordinate: " + worldCol +
                    ", " + worldRow, 3, nl -= 20);
            font.draw(batch, "Original Chunk Position: " + op, 3, nl -= 20);
            font.draw(batch, "Data Position: " + (chunk + op)
                    + ", Col " + col + ", Row " + row, 3, nl -= 20);
            font.draw(batch, GameManager.get().getManager(WorldManager.class).
                    getTileTypeByLocation(1, screenX, screenY).toString(), 3, nl -= 20);

            batch.end();

        }
    }

}
