package com.rexincp.game.graphics;

import com.badlogic.gdx.graphics.Texture;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public enum ResourceTexture {

    // World tiles
    GRASS_TILE(0, "blocks/grass-tile.png"),
    DIRT_TILE(1, "blocks/dirt-tile.png"),
    GOLD_DEPOSIT(2, "blocks/gold-tile.png"),
    COAL_DEPOSIT(3, "blocks/coal-tile.png"),
    FOSSIL_DEPOSIT(4, "blocks/fossil-tile.png"),
    IRON_TILE(5, "blocks/iron-tile.png"),
    STONE_TILE(6, "blocks/stone-tile.png"),
    BEDROCK_TILE(7, "blocks/bedrock.png"),

    // Sky background
    SKY_DARK(8,"background/sky-dark.png"),
    SKY_LIGHT(9,"background/sky-light.png"),
    SKY_GRADIENT(10,"background/sky-gradient.png");

    public static final int TILE_WIDTH = 100;
    public static final int TILE_HEIGHT = 100;
    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceTexture.class);
    private static Map<Integer, ResourceTexture> textureMap;

    private Texture texture;
    private int id;

    /**
     * Constructor
     * @param id ID of texture
     * @param location Hard location on disk
     */
    ResourceTexture(int id, String location) {
        try {
            texture = new Texture(String.format("resources/" + location));
        } catch (Exception e) {
            texture = new Texture("resources/BADTEXTURE.png");
        }
        this.id = id;
    }

    /**
     * Get Texture
     * @return Texture
     */
    public Texture getTexture() {return texture;}

    /**
     * Get the ID of the texture
     * @return Id
     */
    public int getId() {return id;}

    /**
     * Generate list of all textures so a certain texture can be found
     */
    static {
        textureMap = new HashMap<Integer, ResourceTexture>();
        for (ResourceTexture txt : ResourceTexture.values()) {
            textureMap.put(txt.getId(), txt);
        }
    }
}
