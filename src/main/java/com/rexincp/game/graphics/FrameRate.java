package com.rexincp.game.graphics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * A class for calculating framerate
 *
 * @author Ryan Wilson (@RexinCP)
 * @Credit William Hartman (@williamahartman)
 */
public class FrameRate {

    long lastTimeCounted;
    private float sinceChange;
    private float frameRate;

    /**
     * Constructor
     */
    public FrameRate() {
        lastTimeCounted = TimeUtils.millis();
        sinceChange = 0;
        frameRate = Gdx.graphics.getFramesPerSecond();
    }

    /**
     * Update FrameRate variables
     */
    public void update() {
        long delta = TimeUtils.timeSinceMillis(lastTimeCounted);
        lastTimeCounted = TimeUtils.millis();

        sinceChange += delta;
        if(sinceChange >= 1000) {
            sinceChange = 0;
            frameRate = Gdx.graphics.getFramesPerSecond();
        }
    }

    /**
     * Get currect FrameRate
     * @return FrameRate
     */
    public int getFrameRate() {
        return (int) frameRate;
    }
}
