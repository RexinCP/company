package com.rexincp.game.objects;

import com.badlogic.gdx.graphics.Texture;
import com.rexincp.game.graphics.ResourceTexture;

import java.util.HashMap;

/**
 * A=n enum which holds a list of block types which can be rendered
 * or placed in the world. Each block has different attributes and
 * also corresponds with a resource texture.
 *
 * @author Ryan Wilson (@RexinCP)
 */
public enum TileType {

    /**
     * Blocks which can be rendered or placed in the world.
     */
    BLANK(0,false,"Null", null),
    GRASS(1, true, "Grass", ResourceTexture.GRASS_TILE),
    DIRT(2, true, "Dirt", ResourceTexture.DIRT_TILE),
    STONE(3, true, "Stone", ResourceTexture.STONE_TILE),
    M_COAL(6, true, "Coal Deposit", ResourceTexture.COAL_DEPOSIT),
    M_IRON(7, true, "Iron Deposit", ResourceTexture.IRON_TILE),
    M_FOSSIL(8, true, "Fossil", ResourceTexture.FOSSIL_DEPOSIT),
    M_GOLD(9, true, "Gold Deposit", ResourceTexture.GOLD_DEPOSIT),
    BEDROCK(10, true, "Bedrock", ResourceTexture.BEDROCK_TILE),
    SKY_DARK(11, false, "Sky_dark", ResourceTexture.SKY_DARK),
    SKY_LIGHT(12,false, "Sky_light", ResourceTexture.SKY_LIGHT),
    SKY_GRADIENT(13,false, "Sky_Gradient", ResourceTexture.SKY_GRADIENT);

    /* Tile functions */
    public static final int TILE_SIZE = 100;

    private static HashMap<Integer, TileType> tileMap;

    private int id;
    private boolean collidable;
    private String name;
    private int damage;
    private int health;
    private ResourceTexture rt;

    /**
     * Constructor, requires a minimum of 3 parameters
     *
     * @param id Block ID
     * @param collidable If objects will collide with it
     * @param name Name of the tile
     */
    TileType(int id, boolean collidable, String name, ResourceTexture rt) {
        this(id, collidable, name, rt,0);
    }

    /**
     * Same as above, except:
     * @param damage Damage which if in contact with an entity, can inflict damage
     */
    TileType(int id, boolean collidable, String name, ResourceTexture rt, int damage) {
        this(id,collidable,name, rt, damage,10);
    }

    /**
     * Same As above, except:
     * @param health How much damage the
     */
    TileType(int id, boolean collidable, String name, ResourceTexture rt, int damage, int health) {
        this.id = id;
        this.collidable = collidable;
        this.name = name;
        this.damage = damage;
        this.health = health;
        this.rt = rt;
    }

    /**
     * Get ID of the tile
     * @return Tile ID
     */
    public int getId() {
        return id;
    }

    /**
     * Whether the tile is collidable with entities
     * @return If block collidable
     */
    public boolean isCollidable() {
        return collidable;
    }

    /**
     * Get the name of the block
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * Get's the damage the block does to entities 
     * (e.g. Lava would do damage)
     * @return damage
     */
    public int getDamage() {
        return damage;
    }

    /**
     * Get's the health of the tile
     * @return health
     */
    public int getHealth() {
        return health;
    }

    /**
     * Get's the health of the block
     * @param newhealth Health amount to set to
     * @return New Health
     */
    public int setHealth(int newhealth) {
        health = newhealth;
        return health;
    }

    /**
     * Get's the health of the block
     * @param amount amount to change by
     * @return New Health
     */
    public int changeHealth(int amount) {
        health += amount;
        return health;
    }

    /**
     * Get the resource texture of the block
     * @return Texture
     */
    public Texture getTexture() {
        return rt.getTexture();
    }

    /**
     * Get's the TileType depending on the ID
     * @implNote This is used to get the TileType from a generated id
     * @param id ID of the block
     * @return TileType with ID given
     */
    public static TileType getTileTypeById (int id) {
        return tileMap.get(id);
    }

    /**
     * toString() method
     */
    public String toString() {
        return String.format("Tile - ID: %d, Name: %s", id, name);
    }

    /**
     * Add all the Tiles to a list, to then be checked by getTileTypeById
     */
    static {
        tileMap = new HashMap<Integer, TileType>();
        for (TileType tt : TileType.values()) {
            tileMap.put(tt.getId(), tt);
        }
    }

}