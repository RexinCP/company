package com.rexincp.game.world;

import com.rexincp.game.objects.TileType;
import com.rexincp.game.util.Direction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class Chunk {

    // final data
    private final static Logger LOGGER = LoggerFactory.getLogger(Chunk.class);

    // chunk information
    Biome biome;
    private int[][][] data;
    private boolean chunk_loaded;
    private Chunk last_chunk;
    private Direction direction;

    // Generation information
    public boolean mountainRemaining = false;
    public int[][] mountainRemainingData;

    /**
     * Chunk constructor
     * @param biome Biome type of the chunk
     * @param last_chunk Previous chunk data
     * @param direction Direction from the previous chunk. LEFT or RIGHT or INVALID
     */
    public Chunk(Biome biome, Chunk last_chunk, Direction direction) {
        LOGGER.info("Creating new Chunk...");
        this.data = new int[2][ChunkInfo.getHEIGHT()][ChunkInfo.getWIDTH()];
        this.chunk_loaded = false;
        this.biome = biome;
        this.last_chunk = last_chunk;
        this.direction = direction;
        generateChunk();
    }

    /**
     * Generate a chunk randomly
     * TODO Implement previous chunk continual generation
     * TODO Implement seed generation
     */
    private void generateChunk() {

        for (int row = 0; row < ChunkInfo.getHEIGHT(); row++) {
            for (int col = 0; col < ChunkInfo.getWIDTH(); col++) {

                // If bottom 5 rows, delegate bedrock
                if (row == 0) {
                    data[1][row][col] = TileType.BEDROCK.getId();
                } else if (row < ChunkInfo.getSeaLevel() - (ChunkInfo.getHEIGHT() * 0.01)) {
                        if (scatter(col, row, 0, TileType.BEDROCK)) {
                            data[1][row][col] = TileType.BEDROCK.getId();
                        } else {
                            data[1][row][col] = TileType.STONE.getId();
                        }
                } else if (row < ChunkInfo.getSeaLevel()) {
                    if (scatter(col, row, (int)(ChunkInfo.getSeaLevel() - (ChunkInfo.getHEIGHT() * 0.01)), TileType.STONE)) {
                        data[1][row][col] = TileType.STONE.getId();
                    } else {
                        data[1][row][col] = biome.getDefault_block();
                    }
                } else if (row == ChunkInfo.getSeaLevel()) {
                    data[1][row][col] = biome.getDefault_block();
                } else
                    break; // Save Generation time.
            }
        }

        LOGGER.info("Generating mountains....");
        generateMountain();
    }

    /**
     * Randomly Generate a mountain depending on biome information.
     */
    private void generateMountain() {

        int[][] new_mountain = null;
        int mountain_col = 0;
        int mountain_row, row;

        for (int col = 0; col < ChunkInfo.getWIDTH(); col++) {

            row = ChunkInfo.getSeaLevel()+1;

            if (new_mountain == null) {

                if (chanceGen(biome.getMountainChance())) {
                    new_mountain = buildMountain(biome.getDefault_block());
                    col--;
                } else {
                    data[1][row][col] = biome.getSoil();
                }


            } else {
                for (mountain_row = 0; mountain_row < new_mountain.length; mountain_row++, row++) {

                    if (new_mountain[mountain_row][mountain_col] != 0)
                        data[1][row][col] = new_mountain[mountain_row][mountain_col];
                    new_mountain[mountain_row][mountain_col] = 0;
                }


                if ((new_mountain[0].length + col) >= ChunkInfo.getWIDTH() && col == ChunkInfo.getWIDTH()-1) {
                    mountainRemaining = true;
                    mountainRemainingData = new_mountain;
                }

                // Reset mountain data.
                if (mountain_col == (new_mountain[0].length - 1)) {
                    new_mountain = null;
                    mountain_col = 0;
                }
                mountain_col++; // Increase mountain column
            }
        }
    }

    /**
     * Generate and build the data of a mountain randomly
     * TODO Implement seed generation
     * @param tile_id Tile id of the block type to build the mountain out of
     * @return The mountain data
     */
    private int[][] buildMountain(int tile_id) {

        int mountain_height = 3;
        int mountain_width = biome.getMinMountain() +
                ChunkInfo.randomOpt(biome.getMaxMountain() - biome.getMinMountain());
        boolean stepAgain = true;
        int mountain_data[][];
        boolean built;
        int total_count;

        // Generate how high the mountain is going to be.
        while (stepAgain) {
            stepAgain = chanceGen(biome.getMountainChance());
            mountain_height++;

            if (mountain_height > 10)
                break;
        }

        // Initialise data.
        mountain_data = new int[mountain_height][mountain_width];

        // Generate mountain
        for (int row = 0; row < mountain_height; row++) {
            built = false;
            total_count = 0;
            for (int col = 0; col < mountain_width; col++) {
                if (row == 0) {
                    mountain_data[row][col] = tile_id;
                } else if (row == mountain_height-1 && mountain_data[row-1][col] == tile_id) {
                    mountain_data[row][col] = biome.getSoil();
                } else if (mountain_data[row-1][col] == tile_id) {

                    // Far left and right side of each row
                    if ((col > 0 && col < mountain_width-1 && mountain_data[row-1][col-1] != tile_id &&
                            mountain_data[row-1][col+1] != tile_id) || col == 0 || col == mountain_width-1) {
                        built = false;
                        if (chanceGen(biome.getMountainChance()/2))
                            built = true;
                    }

                    if (built) {
                        if (chanceGen(10) && total_count > mountain_width - row * (biome.getMountainChance() / 10)) {
                            mountain_data[row][col] = biome.getSoil();
                            built = false;
                        } else {
                            mountain_data[row][col] = tile_id;
                            total_count++;
                        }
                    } else {
                        if (chanceGen(biome.getMountainChance())) {
                            built = true;
                            total_count = 0;
                            mountain_data[row][col] = tile_id;
                        } else {
                            mountain_data[row][col] = biome.getSoil();
                        }
                    }
                }
            }
        }

        // return mountain width
        return mountain_data;
    }

    /**
     * Generate tunnels
     * TODO Implement seed generation
     */
    private void generateTunnels() {

    }

    /**
     * Generate Ore randomly
     * TODO Implement seed generation
     */
    private void generateOre() {

    }

    /**
     * Scatter the blocks between layers
     * @param col Col of the tile being checked
     * @param row Row of the tile being checked
     * @param start_height Starting height in the world
     * @param check_tile The tile to check against (Tile's being placed)
     * @return Wether the block should be made ot not
     * @throws NullPointerException If check_tile is null
     */
    private boolean scatter(int col, int row, int start_height, TileType check_tile) throws NullPointerException {

        if (check_tile == null)
            throw new NullPointerException();

        final int dec_val = (int) (10 - Math.sqrt(ChunkInfo.getHEIGHT())/10) + 1;
        final int low_chance = 20 - (row - start_height) * dec_val;
        final int med_chance = 50 - (row - start_height) * dec_val;
        final int high_chance = 80 - (row - start_height) * dec_val;

        // TODO change these statements to check for previous chunk
        // if block beneath is not the same, return false
        if (data[1][row-1][col] != check_tile.getId())
            return false;

        // Do not generate if climb/drop is > 2 -> LEFT
        if (col != 0 && row > start_height + 1 && data[1][row-2][col-1] != check_tile.getId())
            return false;

        // Do not generate if climb/drop is > 2 -> RIGHT
        if (col != ChunkInfo.getHEIGHT()-1 && row > start_height + 1 && data[1][row-2][col+1] != check_tile.getId())
            return false;

        // Generate possibility
        if (col == 0) {
            // If far left-side of map, and tile down and to the right is the same block type
            if (data[1][row-1][col+1] == check_tile.getId())
                return chanceGen(med_chance);
            return chanceGen(low_chance);
        } else if (col == ChunkInfo.getWIDTH()-1) {
            // If far right-side of map, and tile down and to the left is the same block type
            if (data[1][row][col-1] == check_tile.getId())
                return chanceGen(high_chance);
            return chanceGen(low_chance);
        } else {
            if (col > 1 && data[1][row][col-2] != check_tile.getId() && data[1][row][col-1] == check_tile.getId()) {
                // If the block 2 to the left is not the same, but the block immediately to the left is.
                return true;
            } else if (data[1][row-1][col+1] == check_tile.getId() && data[1][row][col-1] == check_tile.getId()) {
                // If the block underneath and to the right, and the block immediately to the left.
                return chanceGen(high_chance);
            } else if (data[1][row-1][col+1] == check_tile.getId() && data[1][row][col-1] == check_tile.getId() &&
                    col < ChunkInfo.getWIDTH()-2 && data[1][row-1][col+2] == check_tile.getId()) {
                // If the block underneath and to the right, underneath and 2 to the right,
                // and the block immediately to the left.
                return chanceGen(high_chance);
            } else if (data[1][row-1][col+1] == check_tile.getId()) {
                // Block underneath and to the right.
                return chanceGen(med_chance);
            } else if (data[1][row][col-1] == check_tile.getId()) {
                // Block immediately to the left.
                return chanceGen(med_chance);
            }
        }
        return chanceGen(med_chance);
    }

    /**
     * Return true or false depending on whether the random number is < than the given chance.
     * @param chance % Chance (0 <= chance <= 100).
     * @return If the random number is < the chance.
     */
    private boolean chanceGen(int chance) {
        // Assign chance to be within 0 and 100
        if (chance < 0) {
            chance = 0;
        } else if (chance > 100) {
            chance = 100;
        }

        // Random number between 0 and 100
        Random rand = new Random();
        int randomNumber = rand.nextInt(101);

        return (randomNumber < chance);
    }

    /**
     * Get the chunk data
     * @return All the chunk data
     */
    public int[][][] getData() {
        return data;
    }
}
