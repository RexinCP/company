package com.rexincp.game.world;

import com.rexincp.game.objects.TileType;

import java.util.Random;

/**
 * Class which takes care of all final Chunk Information.
 */
public class ChunkInfo {

    private final static int HEIGHT = 100;
    private final static int WIDTH = 100;
    private final static int SEA_LEVEL = (int) (HEIGHT * 0.8);
    private final static int PIXELWIDTH = WIDTH * TileType.TILE_SIZE;
    private final static int PIXELHEIGHT = HEIGHT * TileType.TILE_SIZE;

    /**
     * Get the height of Sea Level
     * @return Sea Level
     */
    public static int getSeaLevel() {
        return SEA_LEVEL;
    }

    /**
     * Get the width of the chunk in blocks
     * @return Chunk width
     */
    public static int getWIDTH() {
        return WIDTH;
    }

    /**
     * Get the height of the chunk in blocks
     * @return Chunk height
     */
    public static int getHEIGHT() {
        return HEIGHT;
    }

    /**
     * Get the width of the chunk in pixels
     * @return Pixel width
     */
    public static int getPixelWidth() {
        return PIXELWIDTH;
    }

    /**
     * GEt the height of the chunk in pixels
     * @return Pixel height
     */
    public static int getPixelHeight() {
        return PIXELHEIGHT;
    }

    /**
     * Given an amount of arguments, randomly select one of them.
     * @param options Amount of options.
     * @return Which option has been chosen.
     */
    public static int randomOpt(int options) {
        if (options < 1)
            options = 1;

        Random rand = new Random();
        int randomNumber = rand.nextInt(options);
        return randomNumber;
    }
}
