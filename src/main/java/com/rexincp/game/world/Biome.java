package com.rexincp.game.world;

import com.rexincp.game.objects.TileType;

import java.util.HashMap;

/**
 * An enum which contains a list of biomes and certian information about each biome which
 * influences what mountains are formed, and how the biome is generated
 *
 * @author Ryan Wilson (@RexinCP)
 */
public enum Biome {

    PLAIN(0, "Plains", 10, 15, 25, TileType.DIRT, TileType.GRASS),
    DESERT(1, "Desert", 60, 30, 50, TileType.M_FOSSIL, TileType.GRASS),
    HILLS(2, "Mountain", 80, 20, 40, TileType.DIRT, TileType.GRASS),
    JUNGLE(3, "Jungle", 40, 5, 20, TileType.DIRT, TileType.GRASS),
    ARCTIC(4, "Arctic", 5, 8, 30, TileType.DIRT, TileType.GRASS);

    private static HashMap<Integer, Biome> biomeMap;
    private int id;
    private int chance;
    private String name;
    private int min_mountain;
    private int max_mountain;
    private int default_block;
    private int soil;

    /**
     * Constructor
     * @param id ID of the biome
     * @param name Name of the biome
     * @param mountainChance Chance of creating a mountain
     * @param min_mountain Minimum mountain sizee
     * @param max_mountain MAximum mountain size
     * @param default_block Default block of the biome
     * @param soil Soil block
     */
    Biome(int id, String name, int mountainChance, int min_mountain,
          int max_mountain, TileType default_block, TileType soil) {
        this.id = id;
        this.name = name;
        this.chance = mountainChance;
        this.min_mountain = min_mountain;
        this.max_mountain = max_mountain;
        this.default_block = default_block.getId();
        this.soil = soil.getId();

        if (chance < 0) {
            this.chance = 0;
        } else if (chance > 100) {
            this.chance = 100;
        }
    }

    /**
     * GEt the ID of the biome
     * @return ID
     */
    public int getId() {
        return id;
    }

    /**
     * Get the name of the biome.
     * @return Name
     */
    public String getName() {
        return name;
    }

    /**
     * Get's the mountain chance
     * @return Chance of creating a mountain
     */
    public int getMountainChance() {
        return chance;
    }

    /**
     * GEt the minimum size of a mountain
     * @return Min mountain size
     */
    public int getMinMountain() {
        return min_mountain;
    }

    /**
     * Get the maximum size of a mountain
     * @return Max mountain size
     */
    public int getMaxMountain() {
        return max_mountain;
    }

    /**
     * Get the default block of the main ground in the biome
     * @return Default block
     */
    public int getDefault_block() {
        return default_block;
    }

    /**
     * Get the Default soil block
     * @return Soil block
     */
    public int getSoil() {
        return soil;
    }

    /**
     * Get the biome, givcen it's ID
     * @param id id of the biome
     * @return Biome
     */
    public static Biome getBiomeById (int id) {
        return biomeMap.get(id);
    }

    /**
     * Add all biomes' to a list
     */
    static {
        biomeMap = new HashMap<Integer, Biome>();
        for (Biome b : Biome.values()) {
            biomeMap.put(b.getId(), b);
        }
    }
}
