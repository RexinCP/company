package com.rexincp.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.rexincp.game.graphics.Performance;
import com.rexincp.game.manager.GameManager;
import com.rexincp.game.manager.InputManager;
import com.rexincp.game.manager.WorldManager;
import com.rexincp.game.util.Direction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GameScreen implements Screen {

    private Logger LOGGERR = LoggerFactory.getLogger(GameScreen.class);
    private SpriteBatch batch;
    private InputManager input;
    private Performance performance;
    private long lastTick;
    private WorldManager world;

    @Override
    public void show() {
        // Initialise camera, batch and world
        GameManager.get().initGame(new OrthographicCamera());
        world = new WorldManager();
        world.generateNewChunk(Direction.INVALID);

        batch = new SpriteBatch();
        performance = new Performance();
        initManagers();

        input = new InputManager(GameManager.get().getCamera());
        Gdx.input.setInputProcessor(input);

        lastTick = System.currentTimeMillis();
        LOGGERR.info("Creating Game Screen...");
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        world.render(GameManager.get().getCamera(), batch);

        performance.render(); // Get performance
        gameTick();
    }

    @Override
    public void resize(int width, int height) {
        GameManager.get().getCamera().viewportWidth = Gdx.graphics.getWidth();
        GameManager.get().getCamera().viewportHeight = Gdx.graphics.getHeight();
        GameManager.get().getCamera().update();
        input.checkCamera();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    private void gameTick() {
        if (System.currentTimeMillis() - lastTick > 500) {
            lastTick = System.currentTimeMillis();
            GameManager.get().onTick();
        }
    }

    private void initManagers() {
        GameManager.get().addManager(world);
        GameManager.get().addManager(performance);
    }
}