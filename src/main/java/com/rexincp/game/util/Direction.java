package com.rexincp.game.util;

public enum Direction {
    INVALID(0),
    LEFT(1),
    RIGHT(2),
    DOWN(3),
    UP(4);

    int dir;
    Direction(int dir) {
        this.dir = dir;
    }
}
